import { Component, OnInit } from '@angular/core';
import './js/EnableThreeExamples.js';
import 'three/examples/js/controls/OrbitControls.js';
import { LandscapeService } from '../../services/landscape/landscape.service';
import { SnowFlakeService } from "../../services/snow-flakes/snow-flakes.service";
import {SceneService} from "../../services/scene/scene.service";

@Component({
  selector: 'scene',
  templateUrl: './scene.component.html',
  styleUrls: ['./scene.component.scss']
})

export class SceneComponent implements OnInit {

  constructor(private sceneService: SceneService,
              private landscapeService: LandscapeService,
              private snowFlakeService: SnowFlakeService) {
  }

  ngOnInit() {
    this.sceneService.createScene('sceneCanvas');
    this.landscapeService.addLandscape(this.sceneService.scene);
    this.snowFlakeService.addSnowFlakes(this.sceneService.scene, false);
    this.sceneService.animate();
  }
}
