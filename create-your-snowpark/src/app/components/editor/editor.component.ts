import { Component, OnInit } from '@angular/core';
import { SceneComponent } from '../scene/scene.component';

@Component({
  selector: 'editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})

export class EditorComponent implements OnInit {

  constructor(private sceneComponent: SceneComponent) { }

  ngOnInit() { }
}
