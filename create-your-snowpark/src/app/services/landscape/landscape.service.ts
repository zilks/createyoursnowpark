import { Injectable } from '@angular/core';
import * as THREE from 'three';

@Injectable({
  providedIn: 'root'
})
export class LandscapeService {

  private geometry: THREE.BoxGeometry;
  private material: THREE.MeshPhongMaterial;
  private mesh: THREE.Mesh;

  addLandscape(scene) {
    this.geometry = new THREE.BoxGeometry(50, 1, 100);
    this.material = new THREE.MeshPhongMaterial({color: 0xffffff});

    this.mesh = new THREE.Mesh(this.geometry, this.material);

    scene.add(this.mesh);
  }

  animateLandscape() {
    this.mesh.rotation.x += 0.001;
    this.mesh.rotation.y += 0.002;
  }

}
