import {ElementRef, Injectable} from '@angular/core';
import * as THREE from "three";

@Injectable({
  providedIn: 'root'
})
export class SceneService {

  //https://github.com/JohnnyDevNull/ng-three-template/blob/master/src/app/engine/engine.service.ts

  private sceneCanvas: HTMLCanvasElement;

  public scene: THREE.Scene;

  private renderer: THREE.WebGLRenderer;
  private camera: THREE.PerspectiveCamera;

  private rightLight: THREE.PointLight;
  private leftLight: THREE.PointLight;
  private ambientLight: THREE.AmbientLight;
  private controls: THREE.OrbitControls;

  constructor() { }

  createScene(canvasId: string) {
    this.sceneCanvas = <HTMLCanvasElement>document.getElementById(canvasId);

    this.renderer = new THREE.WebGLRenderer({
      canvas: this.sceneCanvas,
      antialias: true
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0x293133 );

    this.camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 1000);
    this.camera.position.x = -15;
    this.camera.position.y = 30;
    this.camera.position.z = 100;

    this.rightLight = new THREE.PointLight(0xffffff, 0.3, 0);
    this.rightLight.position.set(10, 20, 7);

    this.leftLight = new THREE.PointLight(0xffffff, 0.3, 0);
    this.leftLight.position.set(-10, 20, 7);

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.8);

    this.scene.add(this.rightLight);
    this.scene.add(this.leftLight);
    this.scene.add(this.ambientLight);

    this.controls = new THREE.OrbitControls(this.camera);
    this.controls.update();

    this.render();

    return this.scene;
  }

  animate() {
    window.addEventListener('DOMContentLoaded', () => {
      this.render();
    });

    window.addEventListener('resize', () => {
      this.resize();
    });
  }

  render() {
    requestAnimationFrame(() => {
      this.render();
    });

    this.renderer.render(this.scene, this.camera);
  }

  resize() {
    let width = window.innerWidth;
    let height = window.innerHeight;

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize( width, height );
  }
}
