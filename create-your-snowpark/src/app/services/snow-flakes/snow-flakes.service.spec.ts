import { TestBed } from '@angular/core/testing';

import { SnowFlakeService } from './snow-flakes.service';

describe('SnowFlakesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SnowFlakeService = TestBed.get(SnowFlakeService);
    expect(service).toBeTruthy();
  });
});
