import { Injectable } from '@angular/core';
import * as THREE from 'three';
import {Object3D} from 'three';

@Injectable({
  providedIn: 'root'
})
export class SnowFlakeService {

  /* Snow Flakes */
  private flakeCount: number;
  private flakeGeometry: THREE.TetrahedronGeometry;
  private flakeMaterial: THREE.MeshPhongMaterial;
  private snow: THREE.Group;
  private flakeMesh: THREE.Mesh;
  private flakeArray: Object3D[];

  initSnowFlakes() {
    this.flakeCount = 9000;
    this.flakeGeometry = new THREE.TetrahedronGeometry(0.035);
    this.flakeMaterial = new THREE.MeshPhongMaterial({color: 0xffffff});
    this.snow = new THREE.Group();

    for (let i = 0; i < this.flakeCount; i++) {
      this.flakeMesh = new THREE.Mesh(this.flakeGeometry, this.flakeMaterial);
      this.flakeMesh.position.set(
        (Math.random() - 0.5) * 40,
        (Math.random() - 0.5) * 20,
        (Math.random() - 0.5) * 40
      );
      this.snow.add(this.flakeMesh);
    }

    this.flakeArray = this.snow.children;
  }

  addSnowFlakes(scene, showSnowflakes: boolean) {
    if(showSnowflakes === true) {
      this.initSnowFlakes();
      scene.add(this.snow);
      this.animateSnowflakes();
    }
  }

  animateSnowflakes() {
    for (let i = 0; i < this.flakeArray.length / 2; i++) {
      this.flakeArray[i].rotation.y += 0.01;
      this.flakeArray[i].rotation.x += 0.02;
      this.flakeArray[i].rotation.z += 0.03;
      this.flakeArray[i].position.y -= 0.018;
      if (this.flakeArray[i].position.y < -4) {
        this.flakeArray[i].position.y += 10;
      }
    }

    for (let i = this.flakeArray.length / 2; i < this.flakeArray.length; i++) {
      this.flakeArray[i].rotation.y -= 0.03;
      this.flakeArray[i].rotation.x -= 0.03;
      this.flakeArray[i].rotation.z -= 0.02;
      this.flakeArray[i].position.y -= 0.016;
      if (this.flakeArray[i].position.y < -4) {
        this.flakeArray[i].position.y += 9.5;
      }

      this.snow.rotation.y -= 0.0000002;
    }
  }
}
