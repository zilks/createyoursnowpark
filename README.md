# README #

This application uses the following libraries and frameworks:

* Three.js
* Node.js
* npm
* AngularJS
* Bootstrap


### What is this repository for? ###

* Backup for the source code
* Version


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Lukas Zimmermann

### TODOs ###
- Adding Snowflakes (can be switched on/off via GUI) --> Bugfix needed
- Modify GUI
- Add Obstacle Service
- Maybe: Add Modules for lazy-loading and better architecture